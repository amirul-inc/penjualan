const order = require("../model/orders");
const product = require("../model/products");

module.exports = {
  orderCreated: async (req, res, next) => {
    try {
      const Product = await product
        .findById(req.body.productId)
        .populate("prices");
      const selectedPrice = Product.prices.find(
        (price) => price._id.toString() === req.body.priceId
      );
      if (!selectedPrice) {
        throw new Error("Selected price not found");
      }
      if (product.quantity < req.body.quantity) {
        return res.status(400).json({ error: "Insufficient stock" });
      }
      const totalCost = selectedPrice.value * req.body.quantity;
      const newOrder = new order({
        ...req.body,
        totalCost,
      });
      await product.update(
        {},
        { $set: { quantity: Product.quantity - req.body.quantity } }
      );
      await newOrder.save();
      return res.status(200).json({ message: "Transaction has successfully" });
    } catch (err) {
      return next(err);
    }
  },

  getOrder: async (req, res) => {
    try {
      const newData = [];
      const response = await order.find().populate("productId");
      Array(response)[0].map((item) =>
        newData.push({
          _id: item._id,
          name: item.productId.name,
          description: item.productId.description,
          price: item.productId.prices.filter(
            (data) => data.id == item.priceId
          )[0],
          purchasedAt: item.purchasedAt,
        })
      );
      return res.status(200).json(newData);
    } catch (err) {
      return res.status(400).json({ message: "internal server error" });
    }
  },
};
