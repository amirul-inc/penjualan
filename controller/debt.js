const debt = require("../model/debt");
module.exports = {
  debCreated: async (req, res, next) => {
    try {
      const newDebt = new debt({
        ...req.body,
      });
      await newDebt.save();
      return res.status(200).json({ message: "Add new debt successfully" });
    } catch (error) {
      return next(error);
    }
  },
  debtGet: async (req, res, next) => {
    try {
      const response = await debt.find();
      return res.status(200).json(response);
    } catch (error) {
      return next(error);
    }
  },
  debtGetById: async (req, res, next) => {
    try {
      const { debtId } = req.params;
      const response = await debt.findById(debtId);
      return res.status(200).json(response);
    } catch (error) {
      return next(error);
    }
  },
  debtUpdate: async (req, res, next) => {
    try {
      const { debtId } = req.params;
      const response = await debt.findById(debtId);
      response.fullName = req.body.fullName;
      response.address = req.body.address;
      response.phone = req.body.phone;
      response.total = req.body.total;
      await response.save();
      return res
        .status(200)
        .json({ data: updatedProduct, message: "Update debt successfully" });
      return;
    } catch (error) {
      next(error);
    }
  },
};
