const jwt = require("jsonwebtoken");
const bcrypt = require("bcrypt");
const userModel = require("../model/user");

const validatePhone = async (phone) => {
  let user = await userModel.findOne({ phone });
  return user ? false : true;
};
const validateUsername = async (username) => {
  let user = await userModel.findOne({ username });
  return user ? false : true;
};

module.exports = {
  getAllUser: async (req, res) => {
    try {
      const users = await userModel.find({}).select("-password");
      res.status(201).json(users);
    } catch (err) {
      return res.status(400).json("something wrong");
    }
  },
  Register: async (user, res) => {
    try {
      const userNameNotTaken = await validateUsername(user.email);
      if (!userNameNotTaken) {
        return res.status(400).json({ message: "Username already exists" });
      }
      const phoneNotTaken = await validatePhone(user.phone);
      if (!phoneNotTaken) {
        return res.status(400).json({ message: "Phone already exists" });
      }
      const password = await bcrypt.hash(user.password, 12);
      const newUser = new userModel({
        ...user,
        password,
      });
      await newUser.save();
      return res.status(201).json({ message: "User registered successfully" });
    } catch (err) {
      return res.status(500).json({ message: "Internal server error" });
    }
  },
  Login: async (req, res) => {
    try {
      const { username, password } = req;
      const user = await userModel.findOne({ username });
      if (!user) {
        return res.status(401).json({ message: "Invalid username" });
      }
      const isMatch = await bcrypt.compare(password, user.password);
      if (isMatch) {
        const token = jwt.sign(
          {
            user_id: user._id,
            role: user.role,
            username: user.username,
            phone: user.phone,
          },
          process.env.VERY_SECRET,
          { expiresIn: "24h" }
        );
        return res.json({
          token: `Bearer ${token}`,
          message: "Authentication successful",
        });
      } else {
        return res.status(401).json({
          message: "Invalid password",
        });
      }
    } catch (err) {
      return err;
    }
  },

  Role: (roles) => (req, res, next) =>
    !roles.includes(req.user.role)
      ? res.status(401).json("Unauthorized")
      : next(),
};
