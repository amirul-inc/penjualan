const Purchase = require("../model/purchases");
const Product = require("../model/products");

module.exports = {
  getReporByDate: async (req, res) => {
    try {
      const { year, month, date } = req.params;
      const startDate = new Date(year, month - 1, date);
      const endDate = new Date(year, month - 1, parseInt(date) + 1);
      const response = await Purchase.find({
        date: {
          $gte: startDate,
          $lt: endDate,
        },
      }).populate("items.productId");
      let assets = 0;
      let potential = 0;
      const getProduct = await Product.find();
      for (let i = 0; i < getProduct.length; i++) {
        const getPotentialAsset =
          getProduct[i].prices.sort((a, b) => b.price - a.price)[0].value *
          getProduct[i].stock;
        const getAsset = getProduct[i].purchasePrice * getProduct[i].stock;
        assets += getAsset;
        potential += getPotentialAsset;
      }
      let total = 0;
      let purchase = 0;
      for (let i = 0; i < response.length; i++) {
        const totalIncome = response[i].total;
        total += totalIncome;
        for (let y = 0; y < response[i].items.length; y++) {
          const getTotatlPurchase = Math.ceil(
            response[i].items[y].productId.purchasePrice *
              response[i].items[y].quantity
          );
          purchase += getTotatlPurchase;
        }
      }
      const newResponse = {
        detail: response,
        totalTransaction: response.length,
        totalIncome: total,
        purchasePrice: purchase,
        profit: total - purchase,
        totalAssets: assets,
        potentialAssets: potential,
      };
      return res.status(200).json(newResponse);
    } catch (err) {
      return res.status(400).json({ message: "internal server error" });
    }
  },
  getReporByMonth: async (req, res) => {
    try {
      const { year, month, date } = req.params;
      const startDate = new Date(year, month - 1, 1);
      const endDate = new Date(year, month, 1);
      const response = await Purchase.find({
        date: {
          $gte: startDate,
          $lt: endDate,
        },
      }).populate("items.productId");
      let assets = 0;
      let potential = 0;
      const getProduct = await Product.find();
      for (let i = 0; i < getProduct.length; i++) {
        const getPotentialAsset = Math.ceil(
          getProduct[i].prices.sort((a, b) => b.price - a.price)[0].value *
            getProduct[i].stock
        );
        const getAsset = getProduct[i].purchasePrice * getProduct[i].stock;
        assets += getAsset;
        potential += Math.ceil(getPotentialAsset);
      }
      let total = 0;
      let purchase = 0;
      for (let i = 0; i < response.length; i++) {
        const totalIncome = response[i].total;
        total += totalIncome;
        for (let y = 0; y < response[i].items.length; y++) {
          const getTotatlPurchase = Math.ceil(
            response[i].items[y].productId.purchasePrice *
              response[i].items[y].quantity
          );
          //   console.log(getTotatlPurchase);
          purchase += getTotatlPurchase;
        }
      }
      const newResponse = {
        detail: response,
        totalTransaction: response.length,
        totalIncome: total,
        purchasePrice: purchase,
        profit: total - purchase,
        totalAssets: assets,
        potentialAssets: potential,
      };
      return res.status(200).json(newResponse);
    } catch (err) {
      return res.status(400).json({ message: "internal server error" });
    }
  },
  getReporByYear: async (req, res) => {
    try {
      const { year, month, date } = req.params;
      const startDate = new Date(year, 0, 1);
      const endDate = new Date(parseInt(year) + 1, 0, 1);
      const response = await Purchase.find({
        date: {
          $gte: startDate,
          $lt: endDate,
        },
      }).populate("items.productId");
      let assets = 0;
      let potential = 0;
      const getProduct = await Product.find();
      for (let i = 0; i < getProduct.length; i++) {
        const getPotentialAsset = Math.ceil(
          getProduct[i].prices.sort((a, b) => b.price - a.price)[0].value *
            getProduct[i].stock
        );
        const getAsset = getProduct[i].purchasePrice * getProduct[i].stock;
        assets += getAsset;
        potential += getPotentialAsset;
      }
      let total = 0;
      let purchase = 0;
      for (let i = 0; i < response.length; i++) {
        const totalIncome = response[i].total;
        total += totalIncome;
        for (let y = 0; y < response[i].items.length; y++) {
          const getTotatlPurchase = Math.ceil(
            response[i].items[y].productId.purchasePrice *
              response[i].items[y].quantity
          );
          console.log(getTotatlPurchase);
          purchase += getTotatlPurchase;
        }
      }
      const newResponse = {
        detail: response,
        totalTransaction: response.length,
        totalIncome: total,
        purchasePrice: purchase,
        profit: total - purchase,
        totalAssets: assets,
        potentialAssets: potential,
      };
      return res.status(200).json(newResponse);
    } catch (err) {
      return res.status(400).json({ message: "internal server error" });
    }
  },
};
