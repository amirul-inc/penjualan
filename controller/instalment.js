const instalment = require("../model/instalment");
const debts = require("../model/debt");
module.exports = {
  instalmentCreated: async (req, res, next) => {
    try {
      const debt = await debts.find({ debt: req.body.debt });
      console.log(debt);
      if (debt) {
        const newInstalment = new instalment({
          ...req.body,
        });
        await newInstalment.save();
        return res
          .status(200)
          .json({ message: "Add new instalment successfully" });
      } else {
        return res.status(400).json({ message: "Debt Notfound!!" });
      }
    } catch (error) {
      return next(error);
    }
  },
  instalmentGetByDebtId: async (req, res, next) => {
    try {
      const { debtId } = req.params;
      const response = await instalment.find({ debt: debtId }).populate("debt");
      return res.status(200).json(response);
    } catch (error) {
      return next(error);
    }
  },
};
