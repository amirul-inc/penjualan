const path = require("path");
const fs = require("fs");
const product = require("../model/products");

module.exports = {
  productCreated: async (req, res, next) => {
    try {
      const name = await product.findOne({ name: req.body.name });
      if (name) {
        fs.unlink(
          path.join(`./public/images/${req.file.originalname}`),
          (err) => console.log(err)
        );
        return res.status(400).json("Product already exist");
      } else {
        const newProduct = new product({
          ...req.body,
          images: req.file && req.file.path,
        });
        await newProduct.save();
      }
      return res.status(200).json({ message: "Add new product successfully" });
    } catch (err) {
      return next(err);
    }
  },

  productUpdated: async (req, res, next) => {
    try {
      const { productId } = req.params;
      const { name, description, prices, stock, purchasePrice, images } =
        req.body;

      // Find the product by ID
      const response = await product.findById(productId);

      if (!response) {
        return res.status(404).json({ error: "Product not found" });
      }
      // Update the product fields
      response.name = req.body.name;
      response.description = req.body.description;
      response.stock = req.body.stock;
      response.purchasePrice = req.body.purchasePrice;
      response.images = req.file && req.file.path;
      response.updatedAt = Date.now();

      // Update the prices array elements
      if (req.body.prices && Array.isArray(prices)) {
        response.prices = prices.map((price) => ({
          name: price.name,
          value: price.value,
        }));
      }
      // Save the updated product
      const updatedProduct = await response.save();
      return res
        .status(200)
        .json({ data: updatedProduct, message: "Update product successfully" });
    } catch (error) {
      next(error);
    }
  },

  getProduct: async (req, res) => {
    const { page = 1, limit = 10, searchKey = "" } = req.query;
    try {
      if (searchKey !== "") {
        const products = await product
          .find({ $text: { $search: searchKey } })
          .limit(limit * 1)
          .skip((page - 1) * limit)
          .exec();
        const count = await product.countDocuments();
        res.status(200).json({
          products,
          totalPages: Math.ceil(count / limit),
          currentPage: page,
        });
      } else {
        const products = await product
          .find()
          .limit(limit * 1)
          .skip((page - 1) * limit)
          .exec();
        const count = await product.countDocuments();
        res.status(200).json({
          products,
          totalPages: Math.ceil(count / limit),
          currentPage: page,
        });
      }
    } catch (err) {
      return res.status(400).json({ message: "internal server error" });
    }
  },

  getProductById: async (req, res) => {
    try {
      const { productId } = req.params;
      const response = await product.findById(productId);
      res.status(200).json({
        response,
      });
    } catch (err) {
      return res.status(400).json({ message: "internal server error" });
    }
  },
};
