const Purchase = require("../model/purchases");
const Product = require("../model/products");
module.exports = {
  purchaseCreated: async (req, res, next) => {
    try {
      const { items, total } = req.body;
      let totalPrice = 0;
      for (let i = 0; i < req.body.items.length; i++) {
        const productId = items[i].productId;
        const product = await Product.findById(productId);
        const requestedQuantity = req.body.items[i].quantity;
        if (requestedQuantity > product.quantity) {
          return res
            .status(400)
            .json({ error: `Insufficient stock for product ${product.name}` });
        }
        const productPrice = Math.ceil(items[i].value * items[i].quantity);
        totalPrice += productPrice;
        product.stock -= req.body.items[i].quantity;
        await product.save();
      }
      const purchase = new Purchase({
        items,
        total: totalPrice,
      });
      await purchase.save();

      return res.status(201).json(purchase);
    } catch (error) {
      console.log(error);
      next(error);
    }
  },
  getPurchases: async (req, res) => {
    try {
      const response = await Purchase.find()
        .sort("-date")
        .populate("items.productId");
      return res.status(200).json(response);
    } catch (err) {
      return res.status(400).json({ message: "internal server error" });
    }
  },
  getPurchaseById: async (req, res) => {
    try {
      const { transactioId } = req.params;
      const response = await Purchase.findById(transactioId)
        .sort("-date")
        .populate("items.productId");
      return res.status(200).json(response);
    } catch (err) {
      return res.status(400).json({ message: "internal server error" });
    }
  },
  getPurchasesByDate: async (req, res) => {
    try {
      const { year, month, date } = req.params;
      const startDate = new Date(year, month - 1, date);
      const endDate = new Date(year, month - 1, parseInt(date) + 1);
      const response = await Purchase.find({
        date: {
          $gte: startDate,
          $lt: endDate,
        },
      })
        .sort("-date")
        .populate("items.productId");
      let total = 0;
      for (let i = 0; i < response.length; i++) {
        const totalIncome = response[i].total;
        total += totalIncome;
      }
      const newResponse = {
        detail: response,
        totalTransaction: response.length,
        totalIncome: total,
      };
      return res.status(200).json(newResponse);
    } catch (err) {
      return res.status(400).json({ message: "internal server error" });
    }
  },
  getPurchasesByMonth: async (req, res) => {
    try {
      const { year, month, date } = req.params;
      const startDate = new Date(year, month - 1, 1);
      const endDate = new Date(year, month, 1);
      const response = await Purchase.find({
        date: {
          $gte: startDate,
          $lt: endDate,
        },
      })
        .sort("-date")
        .populate("items.productId");
      let total = 0;
      for (let i = 0; i < response.length; i++) {
        const totalIncome = response[i].total;
        total += totalIncome;
      }
      const newResponse = {
        detail: response,
        totalTransaction: response.length,
        totalIncome: total,
      };
      return res.status(200).json(newResponse);
    } catch (err) {
      return res.status(400).json({ message: "internal server error" });
    }
  },
  getPurchasesByYear: async (req, res) => {
    try {
      const { year, month, date } = req.params;
      const startDate = new Date(year, 0, 1);
      const endDate = new Date(parseInt(year) + 1, 0, 1);
      const response = await Purchase.find({
        date: {
          $gte: startDate,
          $lt: endDate,
        },
      })
        .sort("-date")
        .populate("items.productId");
      let total = 0;
      for (let i = 0; i < response.length; i++) {
        const totalIncome = response[i].total;
        total += totalIncome;
      }
      const newResponse = {
        detail: response,
        totalTransaction: response.length,
        totalIncome: total,
      };
      return res.status(200).json(newResponse);
    } catch (err) {
      return res.status(400).json({ message: "internal server error" });
    }
  },
};
