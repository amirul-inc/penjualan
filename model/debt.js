const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const debtSchema = new Schema(
  {
    fullName: {
      type: String,
      require: true,
    },
    address: {
      type: String,
    },
    phone: {
      type: String,
    },
    total: {
      type: Number,
      require: true,
    },
  },
  {
    timestamps: true,
  }
);
module.exports = mongoose.model("Debt", debtSchema);
