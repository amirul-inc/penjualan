const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const instalmentSchema = new Schema({
  debt: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "Debt",
    require: true,
  },
  value: {
    type: Number,
    required: true,
  },
  createdAt: {
    type: Date,
    default: Date.now,
  },
});
module.exports = mongoose.model("instalment", instalmentSchema);
