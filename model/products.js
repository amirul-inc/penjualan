const mongoose = require("mongoose");

const priceSchema = new mongoose.Schema({
  value: {
    type: Number,
    required: true,
  },
  name: {
    type: String,
    required: true,
  },
});

const productSchema = new mongoose.Schema({
  name: {
    type: String,
    required: true,
  },
  description: {
    type: String,
    required: true,
  },
  prices: [priceSchema],
  stock: {
    type: Number,
    required: true,
    default: 0,
  },
  purchasePrice: {
    type: Number,
    required: true,
  },
  images: {
    type: String,
  },
  createdAt: {
    type: Date,
    default: Date.now,
  },
  updatedAt: {
    type: Date,
    default: Date.now,
  },
});
productSchema.index({ "$**": "text" });
const Product = mongoose.model("Product", productSchema);

module.exports = Product;
