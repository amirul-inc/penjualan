const jwt = require("jsonwebtoken");

const authorization = (req, res, next) => {
  const header = req.headers.authorization;
  if (!header) {
    const errorResponse = {
      message: `Unauthorized`,
    };
    return res.status(400).json(errorResponse);
  }
  const token = header.split(" ")[1];
  jwt.verify(token, process.env.VERY_SECRET, (error) => {
    if (error) {
      const errorResponse = {
        message: `Unauthorized: ${error.message}`,
      };
      return res.status(400).json(errorResponse);
    } else {
      next();
    }
  });
};

module.exports = authorization;
