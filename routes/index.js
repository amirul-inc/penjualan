const express = require("express");
const router = express.Router();
const produk = require("../controller/products");
const { upload } = require("../middleware/upload");
const authorization = require("../middleware/authorization");

router.get("/list", produk.getProduct);
router.get("/:productId", produk.getProductById);
router.post("/add", upload.single("images"), produk.productCreated);
router.put("/:productId", upload.single("images"), produk.productUpdated);
// router.delete("/delete/:id", produk.deleteProductById);

module.exports = router;
