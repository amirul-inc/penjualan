const express = require("express");
const router = express.Router();
const reporting = require("../controller/reporting");
const authorization = require("../middleware/authorization");

router.get("/:year/:month/:date", reporting.getReporByDate);
router.get("/:year/:month", reporting.getReporByMonth);
router.get("/:year", reporting.getReporByYear);

module.exports = router;
