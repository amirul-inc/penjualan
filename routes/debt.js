const express = require("express");
const router = express.Router();
const debt = require("../controller/debt");
const instalment = require("../controller/instalment");
const authorization = require("../middleware/authorization");

router.get("/", debt.debtGet);
router.get("/:debtId", debt.debtGetById);
router.post("/", debt.debCreated);
router.put("/:debtId", debt.debtUpdate);

router.post("/instalment", instalment.instalmentCreated);
router.get("/instalment/:debtId", instalment.instalmentGetByDebtId);

module.exports = router;
