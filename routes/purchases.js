const express = require("express");
const router = express.Router();
const purchases = require("../controller/purchases");
const authorization = require("../middleware/authorization");

router.get("/", authorization, purchases.getPurchases);
router.get("/:transactionId", purchases.getPurchaseById);
router.get("/:year/:month/:date", purchases.getPurchasesByDate);
router.get("/:year/:month", purchases.getPurchasesByMonth);
router.get("/:year", purchases.getPurchasesByYear);
router.post("/", purchases.purchaseCreated);
// router.put("/edit/:id", produk.updateProductById);
// router.delete("/delete/:id", produk.deleteProductById);

module.exports = router;
