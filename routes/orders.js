const express = require("express");
const router = express.Router();
const order = require("../controller/orders");
const authorization = require("../middleware/authorization");

router.get("/", authorization, order.getOrder);
router.post("/", authorization, order.orderCreated);
// router.put("/edit/:id", produk.updateProductById);
// router.delete("/delete/:id", produk.deleteProductById);

module.exports = router;
