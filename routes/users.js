const express = require("express");
const router = express.Router();
const userUtils = require("../controller/user");
const authorization = require("../middleware/authorization");

/* POST users register listing. */
router.post("/register", async (req, res) => {
  await userUtils.Register(req.body, res);
});

router.get("/list", authorization, userUtils.getAllUser);

/* POST users login listing. */
router.post("/login", async (req, res) => {
  await userUtils.Login(req.body, res);
});

module.exports = router;
