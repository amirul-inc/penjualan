var express = require("express");
var path = require("path");
var cookieParser = require("cookie-parser");
const bodyParser = require("body-parser");
var logger = require("morgan");
const cors = require("cors");
const mongoose = require("mongoose");
const passport = require("passport");
require("dotenv").config();

var indexRouter = require("./routes/index");
var usersRouter = require("./routes/users");
var productRouter = require("./routes/products");
var orderRouter = require("./routes/orders");
var purchaseRouter = require("./routes/purchases");
var reportRouter = require("./routes/reporting");
var debtRouter = require("./routes/debt");

var app = express();

const mongodConnect = process.env.MONGOLAB_URI;

mongoose.connect(mongodConnect, {
  useNewUrlParser: true,
  useUnifiedTopology: true,
  useCreateIndex: true,
});

app.use(cors());
app.use(logger("dev"));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(bodyParser.json());
app.use("/public", express.static("public"));

app.use(passport.initialize());

require("./middleware/auth")(passport);

app.use(function (err, req, res, next) {
  res.status(err.status || 500);
  res.json({ error: err });
});

app.use("/", indexRouter);
app.use("/users", usersRouter);
app.use("/product", productRouter);
app.use("/order", orderRouter);
app.use("/purchase", purchaseRouter);
app.use("/reporting", reportRouter);

app.use("/debt", debtRouter);

module.exports = app;
